<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ApiControllerTest
 * @package Tests\AppBundle\Controller
 */
class ApiControllerTest extends WebTestCase
{

    public function testPost()
    {
        $client = static::createClient();

        $data = [
            'product_id' => '1',
            'quantity'   => '2',
        ];

        $crawler = $client->request('POST', '/api/cart', $data);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testPostParam()
    {
        $client = static::createClient();

        $data = [
            'product_id' => '1',
            'quantity'   => '20',
        ];

        $result =
            '{"error":{"params":[{"code":"required","message":"This value should be 10 or less.","name":"quantity"}],"type":"invalid_param","message":"Invalid data parameters"}}';

        $crawler = $client->request('POST', '/api/cart', $data);
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertEquals($result, $client->getResponse()->getContent());

    }
}


