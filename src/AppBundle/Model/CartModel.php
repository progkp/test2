<?php

namespace AppBundle\Model;

use AppBundle\Entity\Product;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CartModel
 * @package AppBundle\Model
 */
class CartModel
{

    /**
     * @var Product
     * @Assert\NotBlank()
     */
    private $productId;

    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\Range(
     *     min = 1,
     *     max = 10
     * )
     */
    private $quantity;

    /**
     * @var float
     */
    private $sum = 0;

    /**
     * @return Product
     */
    public function getProductId(): ?Product
    {
        return $this->productId;
    }

    /**
     * @param Product $productId
     * @return CartModel
     */
    public function setProductId(Product $productId): CartModel
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return CartModel
     */
    public function setQuantity(int $quantity): CartModel
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return float
     */
    public function getSum(): float
    {
        return $this->productId->getPrice() * $this->quantity;
    }

    /**
     * @param float $sum
     * @return CartModel
     */
    public function setSum(float $sum): CartModel
    {
        $this->sum = $sum + $this->productId->getPrice() * $this->quantity;

        return $this;
    }
}