<?php

namespace AppBundle\Model;

use AppBundle\Entity\Product;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class FullCartModel
 * @package AppBundle\Model
 */
class FullCartModel
{
    /**
     * @var float
     */
    private $totalSum = 0;

    /**
     * @var int
     */
    private $productsCount = 0;

    /**
     * @var ArrayCollection
     */
    private $products;

    /**
     * FullCartModel constructor.
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * @return float
     */
    public function getTotalSum(): float
    {
        return $this->totalSum;
    }

    /**
     * @param float $totalSum
     * @return FullCartModel
     */
    public function setTotalSum(float $totalSum): FullCartModel
    {
        $this->totalSum = $totalSum;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductsCount(): int
    {
        return $this->productsCount;
    }

    /**
     * @param int $productsCount
     * @return FullCartModel
     */
    public function setProductsCount(int $productsCount): FullCartModel
    {
        $this->productsCount = $productsCount;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param CartModel $cartModel
     * @return FullCartModel
     */
    public function addProduct(CartModel $cartModel): FullCartModel
    {
        $this->totalSum += $cartModel->getSum();
        $this->productsCount += $cartModel->getQuantity();
        $empty = true;

        /** @var CartModel $product */
        foreach ($this->products as $product) {
            if ($product->getProductId()->getId() == $cartModel->getProductId()->getId()) {

                $product->setQuantity($product->getQuantity() + $cartModel->getQuantity());
                $product->setSum($cartModel->getSum());
                $empty = false;
            }
        }

        if ($empty) {
            $this->products->add($cartModel);
        }

        return $this;
    }

    public function deleteProduct(Product $pr): FullCartModel
    {
        /** @var CartModel $product */
        foreach ($this->products as $index => $product) {
            if ($product->getProductId()->getId() == $pr->getId()) {
                if (($quantity = ($product->getQuantity() - 1)) === 0) {
                    $this->products->removeElement($product);
                } else {

                    $product->setQuantity($quantity);
                    $product->setSum($product->getSum() - $pr->getPrice());
                }

                $this->totalSum -= $pr->getPrice();
                $this->productsCount -= 1;
            }
        }

        return $this;
    }
}