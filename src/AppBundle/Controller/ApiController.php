<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Form\Type\CartType;
use AppBundle\Helper\ControllerHelperTrait;
use AppBundle\Model\CartModel;
use AppBundle\Model\FullCartModel;
use AppBundle\Services\CacheService;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ApiController
 * @package AppBundle\Controller
 * @Route("/api")
 */
class ApiController extends FOSRestController
{
    Use ControllerHelperTrait;

    /**
     * @var CacheService
     */
    private $cache;

    /**
     * ApiController constructor.
     * @param CacheService $cache
     */
    public function __construct(CacheService $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @return \FOS\RestBundle\View\View
     *
     * @Route(
     *     "/products",
     *     name="api_product_cget",
     *     methods={"GET"}
     * )
     */
    public function cgetProductAction()
    {
        try {

            if ($products = $this->getDoctrine()->getRepository(Product::class)->findAll()) {

                return $this->ok($products);
            }

            return $this->notFound();

        } catch (\Exception $e) {
            return $this->badRequest($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \FOS\RestBundle\View\View|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Psr\Cache\InvalidArgumentException
     *
     * @Route(
     *     "/cart",
     *     name="api_cart_post",
     *     methods={"POST"}
     * )
     */
    public function postAction(Request $request)
    {
        try {

            $form = $this->createForm(CartType::class, new CartModel());
            $form->submit($request->request->all());
            $key = $this->checkCookie($request);

            if ($form->isValid()) {

                /** @var FullCartModel $cart */
                if (!($cart = $this->cache->checkCache($key))) {
                    $cart = new FullCartModel();
                }

                $cart->addProduct($form->getData());
                $this->cache->updateCache($key, $cart);

                return $this->ok();

            } else {
                return $this->badParam($form);
            }

        } catch (\Exception $e) {
            return $this->badRequest($e->getMessage());
        }

    }

    /**
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     * @throws \Psr\Cache\InvalidArgumentException
     *
     * @Route(
     *     "/cart",
     *     name="api_cart_get",
     *     methods={"GET"}
     * )
     */
    public function getAction(Request $request)
    {
        try {

            $key = $this->checkCookie($request);

            return ($cache = $this->cache->checkCache($key)) ? $this->ok($cache) : $this->ok(new FullCartModel());

        } catch (\Exception $e) {
            return $this->badRequest($e->getMessage());
        }

    }

    /**
     * @param Product $product
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     * @throws \Psr\Cache\InvalidArgumentException
     *
     * @Route(
     *     "/cart/{product}",
     *     name="api_cart_delete",
     *     methods={"DELETE"}
     * )
     */
    public function deleteAction(Product $product, Request $request)
    {
        try {

            $key = $this->checkCookie($request);

            /** @var FullCartModel $cart */
            if ($cart = $this->cache->checkCache($this->checkCookie($request))) {

                $cart->deleteProduct($product);
                $this->cache->updateCache($key, $cart);

                return $this->ok();
            }

        } catch (\Exception $e) {
            return $this->badRequest($e->getMessage());
        }
    }
}
