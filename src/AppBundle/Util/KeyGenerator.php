<?php

namespace AppBundle\Util;

/**
 * Class KeyGenerator
 * @package AppBundle\Util
 */
class KeyGenerator
{
    /**
     * @return string
     */
    public function getCookieKey(): string
    {
        return strtoupper(sha1(random_bytes(20)));
    }
}