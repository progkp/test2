<?php

namespace AppBundle\Serializer\Normalizer;

use AppBundle\Model\CartModel;
use AppBundle\Model\FullCartModel;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class FullCartNormalizer
 * @package AppBundle\Serializer\Normalizer
 */
class FullCartNormalizer implements NormalizerInterface
{

    /**
     * @param mixed $object
     * @param null $format
     * @param array $context
     * @return array|bool|float|int|string
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'total_sum'      => $object->getTotalSum(),
            'products_count' => $object->getProductsCount(),
            'products'       => array_map(
                function (CartModel $cartModel) {
                    return [
                        'id'       => $cartModel->getProductId()->getId(),
                        'quantity' => $cartModel->getQuantity(),
                        'sum'      => $cartModel->getSum(),

                    ];
                },
                iterator_to_array($object->getProducts())
            ),
        ];

    }

    /**
     * @param mixed $data
     * @param null $format
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof FullCartModel;
    }
}