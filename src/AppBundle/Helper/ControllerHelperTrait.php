<?php

namespace AppBundle\Helper;

use AppBundle\Enum\ErrorTypeEnum;
use AppBundle\Util\KeyGenerator;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Trait ControllerHelperTrait
 * @package AppBundle\Helper
 */
trait ControllerHelperTrait
{

    /**
     * @param $data
     * @return View
     */
    public function ok($data = 'Ok'): View
    {
        return View::create(['data' => $data], Response::HTTP_OK);
    }

    /**
     * @return View
     */
    public function notFound(): View
    {
        return View::create([
            'error' => [
                'type'    => ErrorTypeEnum::TYPE_NOT_FOUND,
                'message' => ErrorTypeEnum::getTypeName(ErrorTypeEnum::TYPE_NOT_FOUND),
            ],
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * @param FormInterface $form
     * @return View
     */
    public function badParam(FormInterface $form): View
    {

        $errors = [];
        foreach ($form->getErrors(true, true) as $index => $formError) {
            $errors[$index]['code'] = 'required';
            $errors[$index]['message'] = $formError->getMessage();
            $errors[$index]['name'] = $formError->getOrigin()->getConfig()->getName();
        }

        return View::create([
            'error' => [
                'params'  => $errors,
                'type'    => ErrorTypeEnum::TYPE_INVALID_PARAM,
                'message' => ErrorTypeEnum::getTypeName(ErrorTypeEnum::TYPE_INVALID_PARAM),
            ],
        ], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param string $error
     * @return View
     */
    public function badRequest($error): View
    {
        return View::create([
            'error' => [
                'type'    => ErrorTypeEnum::TYPE_BAD_REQUEST,
                'message' => $error,
            ],
        ], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param Request $request
     * @return mixed|string
     */
    public function checkCookie(Request $request)
    {
        $cookie = $request->cookies;

        if (!$cookie->has('cart')) {

            $key = (new KeyGenerator())->getCookieKey();
            $cookie = new Cookie('cart', $key);
            $response = new Response();
            $response->headers->setCookie($cookie);
            $response->send();
        } else {
            $key = $cookie->get('cart');
        }

        return $key;
    }

}