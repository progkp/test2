<?php

namespace AppBundle\Enum;

/**
 * Class ErrorTypeEnum
 * @package AppBundle\Enum
 */
abstract class ErrorTypeEnum
{
    const
        TYPE_NOT_FOUND = "not_found",
        TYPE_INVALID_PARAM = "invalid_param",
        TYPE_BAD_REQUEST = 'bad_request';

    /** @var array error named type */
    protected static $typeName = [
        self::TYPE_NOT_FOUND     => 'Is empty',
        self::TYPE_INVALID_PARAM => 'Invalid data parameters',
        self::TYPE_BAD_REQUEST   => 'Bad request',
    ];

    /**
     * @param $typeShortName
     * @return string
     */
    public static function getTypeName($typeShortName)
    {
        if (!isset(static::$typeName[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$typeName[$typeShortName];
    }

    /**
     * @return array
     */
    public static function getAvailableTypes()
    {
        return [
            self::TYPE_NOT_FOUND,
            self::TYPE_INVALID_PARAM,
            self::TYPE_BAD_REQUEST,
        ];
    }

}