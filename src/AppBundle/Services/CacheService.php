<?php

namespace AppBundle\Services;

use Psr\Cache\CacheItemPoolInterface;

/**
 * Class CacheService
 * @package AppBundle\Services
 */
class CacheService
{
    /**
     * @var CacheItemPoolInterface
     */
    private $cache;

    /**
     * CacheService constructor.
     * @param CacheItemPoolInterface $cache
     */
    public function __construct(CacheItemPoolInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param $key
     * @return bool|mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function checkCache($key)
    {

        if (false === $this->cache->hasItem($key)) {

            return false;
        }

        return $this->cache->getItem($key)->get();
    }

    /**
     * @param $key
     * @param $data
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function updateCache($key, $data)
    {
        $itemCache = $this->cache->getItem($key);
        $itemCache->set($data);

        $this->cache->save($itemCache);
    }
}